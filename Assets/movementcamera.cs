﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movementcamera : MonoBehaviour {

	public CharacterController player;

	
	public int state;
	public GameObject prepinac;
	private SwitchCamera toLeft;
	public Vector3 cameraOriginal;
	private Vector3 lastPlayerPosition;
	private Vector3 camPos;
	public Vector3 playerOriginal;
	private List<GameObject> prepinace;
	public int total;

	// Use this for initialization
	void Start () {
		lastPlayerPosition = player.transform.position;
		playerOriginal = player.transform.position;
		camPos = transform.position;
		cameraOriginal = transform.position;
		state = 0;
		prepinace = new List<GameObject>();
		foreach(GameObject item in GameObject.FindGameObjectsWithTag("SwitchCamera"))
			prepinace.Add(item);

		total = prepinace.Count;

		toLeft = prepinac.GetComponent<SwitchCamera>();

		

	}
	
	// Update is called once per frame
	void Update () {
		state = 0;
		if(prepinace!=null){
			foreach(GameObject item in prepinace){
			SwitchCamera tmp;
			tmp = item.GetComponent<SwitchCamera>();
			if(tmp.status==true){
				state = 1;
			}

		}
		}

		

		float currentPlayerPositionY = player.transform.position.y;

		if(currentPlayerPositionY < -12.0f||DataBehind.isDead==true) {
			player.transform.position = playerOriginal;
			transform.position = cameraOriginal;
			camPos = transform.position;
			lastPlayerPosition = player.transform.position;
			if(DataBehind.isDead==true) DataBehind.isDead = false;
			
		}



		/*if (toLeft.status == true) 
			state = 1;
		else
			state = 0;*/
		switch(state){
			case 0:
			ForwardBackward();
			break;
			case 1:
			LeftRightAndUpDown();
			break;
			default:
			ForwardBackward();
			break;
		}

		
		

	}

	void ForwardBackward () {
		float tmp;

		if (player.transform.position.z > lastPlayerPosition.z) {
			tmp = player.transform.position.z - lastPlayerPosition.z;
			camPos.z = camPos.z + tmp;
		}

		if (player.transform.position.z < lastPlayerPosition.z) {
			tmp = player.transform.position.z - lastPlayerPosition.z;
			camPos.z = camPos.z + tmp;
		}
		
		/*camPos.y = player.transform.position.y - lastPlayerPosition.y;*/
		lastPlayerPosition = player.transform.position;
		transform.position = camPos;
	}

	void LeftRight () {
		float tmp;

		if (player.transform.position.x > lastPlayerPosition.x) {
			tmp = player.transform.position.x - lastPlayerPosition.x;
			camPos.x = camPos.x + tmp;
		}

		if (player.transform.position.x < lastPlayerPosition.x) {
			tmp = player.transform.position.x - lastPlayerPosition.x;
			camPos.x = camPos.x + tmp;
		}
		
		/*camPos.y = player.transform.position.y - lastPlayerPosition.y;*/
		lastPlayerPosition = player.transform.position;
		transform.position = camPos;
	}

	void LeftRightAndUpDown () {
		float tmp;

		if (player.transform.position.x > lastPlayerPosition.x) {
			tmp = player.transform.position.x - lastPlayerPosition.x;
			camPos.x = camPos.x + tmp;
		}

		if (player.transform.position.y > lastPlayerPosition.y) {
			tmp = player.transform.position.y - lastPlayerPosition.y;
			camPos.y = camPos.y + tmp;
		}

		if (player.transform.position.x < lastPlayerPosition.x) {
			tmp = player.transform.position.x - lastPlayerPosition.x;
			camPos.x = camPos.x + tmp;
		}

		if (player.transform.position.y < lastPlayerPosition.y) {
			tmp = player.transform.position.y - lastPlayerPosition.y;
			camPos.y = camPos.y + tmp;
		}
		
		/*camPos.y = player.transform.position.y - lastPlayerPosition.y;*/
		lastPlayerPosition = player.transform.position;
		transform.position = camPos;
	}


	void OnGUI () {
		GUI.color = Color.black;
		var textArea = new Rect(Screen.width/2,Screen.height -30 ,Screen.width, Screen.height);
		var textArea2 = new Rect(Screen.width/2-120,Screen.height -30 ,Screen.width, Screen.height);
		var textArea3 = new Rect(Screen.width-120 ,Screen.height -30 ,Screen.width, Screen.height);
		
		GUI.Label(textArea, lastPlayerPosition.ToString());
		GUI.Label(textArea2, camPos.ToString());
		GUI.Label(textArea3, DataBehind.PocetMinci.ToString());
	}
}
