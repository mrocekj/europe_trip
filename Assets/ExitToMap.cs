﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitToMap : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		
		foreach(GameObject g in SceneManager.GetActiveScene().GetRootGameObjects()){
            	g.SetActive (false);
            }
			SceneManager.LoadScene("2",  LoadSceneMode.Single);
		}
}

