﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlat : MonoBehaviour {
	public Transform target;
    public float speed;
	private Vector3 originalPos;
	private Vector3 originalPosTarget;
	// Use this for initialization
	void Start () {
		originalPos = transform.position;
		originalPosTarget = target.position;

	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
		if(transform.position==target.position){
			target.position = originalPos;
		}
		if(transform.position==originalPos){
			target.position = originalPosTarget;
		}
		
	}



}
