﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PocitajMince : MonoBehaviour {
	private List<GameObject> mince;
	private int totalCount;
	private int count;

	// Use this for initialization
	void Start () {
		mince = new List<GameObject>();
		foreach(GameObject item in GameObject.FindGameObjectsWithTag("Minca"))
			mince.Add(item);
		totalCount = mince.Count; 
		count = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i<mince.Count; i++){

			if(mince[i].activeInHierarchy==false){
				count++;
				mince.Remove(mince[i]);
			}
		}

				
		
	}

	void OnGUI () {

	GUI.Box(new Rect(10,10,100,25), count + " / " + totalCount.ToString());

}
}
