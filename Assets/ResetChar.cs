﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetChar : MonoBehaviour {

	public GameObject cameraObject;
	private Vector3 cameraOriginal;
	private Vector3 playerOriginal;

	private float originalPlayerPositionY;
	
	// - 13.5
	// Use this for initialization
	void Start () {
		cameraOriginal = cameraObject.transform.position;
		playerOriginal = transform.position;
		originalPlayerPositionY = playerOriginal.y;
	}
	
	// Update is called once per frame
	void Update () {
		CheckPosition();

	}
	void CheckPosition () {
		float currentPlayerPositionY = transform.position.y;

		if(currentPlayerPositionY < -12.0f) {
			transform.position = playerOriginal;
			cameraObject.transform.position = cameraOriginal;
		}


	}

 
}
