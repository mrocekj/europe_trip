﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class animControll : MonoBehaviour {

	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("return")){
			anim.Play("Open");
			foreach(GameObject g in SceneManager.GetActiveScene().GetRootGameObjects()){
            	g.SetActive (false);
            }
			SceneManager.LoadScene("2", LoadSceneMode.Additive);
		}
	}
}
