﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour {

	// Use this for initialization

	public Animator anim;
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetAxis("Horizontal") == 0 || Input.GetAxis("Vertical") == 0){
			anim.SetBool("isRunning", false);
			anim.SetBool("idle", true);
		}	

		if (Input.GetButtonDown("Jump")){
			//jump.Play("Jump");
			anim.SetTrigger("isJumping");
			
		}
		
		if (Input.GetAxis("Vertical") > 0){
	
			transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
			//jump.Play("Running");	
			anim.SetBool("isRunning", true);
			anim.SetBool("idle", false);
		}

		if (Input.GetAxis("Vertical") < 0){
			transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
			//jump.Play("Running");
			anim.SetBool("isRunning", true);
			anim.SetBool("idle", false);
		}
		
		if (Input.GetAxis("Horizontal") > 0){
			transform.rotation = Quaternion.AngleAxis(90, Vector3.up);
			//jump.Play("Running");
			anim.SetBool("isRunning", true);
			anim.SetBool("idle", false);
		}

		if (Input.GetAxis("Horizontal") < 0){
			transform.rotation = Quaternion.AngleAxis(-90, Vector3.up);
			//jump.Play("Running");
			anim.SetBool("isRunning", true);
			anim.SetBool("idle", false);
		}

		
		
	}
	

}
