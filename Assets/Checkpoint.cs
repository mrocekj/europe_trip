﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	public Vector3 cameraPos;

	public GameObject poscamera;
	Movementcamera kamera;

	// Use this for initialization
	void Start () {
		kamera = poscamera.GetComponent<Movementcamera>();
	}
	


	void OnTriggerEnter(Collider other){
		
		kamera.playerOriginal = other.transform.position;
		kamera.cameraOriginal = poscamera.transform.position;
		gameObject.SetActive(false);

	}
}
